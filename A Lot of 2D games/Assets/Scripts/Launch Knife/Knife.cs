﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public float verticalSpeed;
    public GameObject knife;
    
    private bool _launch = false;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _launch = true;
        }

        if (transform.position.y > 0.7)
        {
            Instantiate(knife, transform.position, Quaternion.identity);
        }
        if (_launch)
        {
            transform.position += Vector3.up * (verticalSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            Debug.Log("Touched");
        }
    }
}
